import { Component, OnInit } from '@angular/core';
import { TableDataService } from './table-data.service';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {
  public tableData: Array<any> = [];                        // used to store raw data from json
  public columnNames: Array<string> = [];                      // used to store column names of a json object
  public rowCount: Array<number> = [10, 25, 50, 100, 500];      // no of records per page
  public pages: Array<{ id: number, name: string }> = [];       // page number
  public itemsPerPage: FormControl;
  public pageNo: FormControl;
  public paginatedData: Array<any> = [];
  public jsonData: any;
  constructor(private tableDataService: TableDataService) {
    this.itemsPerPage = new FormControl();
    this.pageNo = new FormControl();
  }

  ngOnInit() {
    // get json object from service
    this.jsonData = this.tableDataService.getTableData();
    this.jsonData.subscribe(data => {
      if (data) {
        this.tableData = data;
        if (this.tableData && this.tableData.length) {
          // get column names from one object in the json
          Object.keys(this.tableData[0]).forEach(key => {
            if (key) {
              this.columnNames.push(key);
            }
          });
        }
        this.itemsPerPage.setValue(10);     // set initial records per page as 10
        this.filterData();
      }
    });

    // on value changes populate pages dropdown
    this.itemsPerPage.valueChanges.subscribe((rowCount) => {
      if (rowCount && this.tableData.length) {
        if (rowCount < this.tableData.length) {
          const pageCount = this.tableData.length / rowCount;
          this.pages = [];
          for (let i = 1; i <= pageCount; i++) {
            this.pages.push({ id: i, name: `Page ${i}` });
          }
        } else if (rowCount >= this.tableData.length) {
          this.pages = [{ id: 1, name: 'Page 1' }];
        }
        this.pageNo.setValue(1);
      }
    });

    // on value change populate data
    this.pageNo.valueChanges.subscribe((pageValue) => {
      if (pageValue) {
        this.filterData();
      }
    });

  }

  // populate data as per rowcount and page number selected
  filterData() {
    const skipPages = parseInt(this.pageNo.value, 10);
    const takePages = parseInt(this.itemsPerPage.value, 10);
    this.paginatedData = this.tableData.slice((skipPages - 1) * takePages, ((skipPages - 1) * takePages + takePages));
  }


  // submit row data and show alert
  public submitData(row) {
    if (row) {
      this.tableDataService.submitData(row.id, row.status).subscribe(response => {
        if (response) {
          const rowData = this.paginatedData.find(x => x.id === response.body.id);
          if (response.body && response.body.message === 'success') {
            alert(`The details of ${rowData.name} are submitted successfully`);
          } else {
            alert(`The submission of ${rowData.name} details is unsuccessfull`);
          }
        }
      });
    }
  }

}
