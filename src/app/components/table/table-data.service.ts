import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TableDataService {
  private jsonPath = 'assets/sample_data.json';
  constructor(private http: HttpClient) { }

  public getTableData(): Observable<any> {
    return this.http.get(this.jsonPath);
  }

  public submitData(id: string, status: string): Observable<HttpResponse<any>> {
    const url = 'api/submit';
    return of(new HttpResponse({
      status: 200,
      body: {
        id,
        message: 'success'
      }
    }));
  }
}
