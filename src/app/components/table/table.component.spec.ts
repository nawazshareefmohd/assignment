import { ComponentFixture, TestBed, inject } from '@angular/core/testing';
import * as jsonFile from '../../../assets/sample_data.json';
import { TableComponent } from './table.component';
import { TableDataService as TableDataService } from './table-data.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { of } from 'rxjs';

describe('TableComponent', () => {
  let component: TableComponent;
  let fixture: ComponentFixture<TableComponent>;
  let tableDataService: TableDataService;
  let http: HttpClient;
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TableComponent],
      providers: [TableDataService],
      imports: [FormsModule, ReactiveFormsModule, HttpClientModule]
    });
    fixture = TestBed.createComponent(TableComponent);
    component = fixture.componentInstance;
    tableDataService = TestBed.get(TableDataService);
    http = TestBed.get(HttpClient);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should filter data according to pagination values', () => {
    component.tableData = (jsonFile as any).default;
    component.pageNo.setValue(1);
    component.itemsPerPage.setValue(10);
    component.filterData();
    expect(component.paginatedData.length).toEqual(10);
  });

  it('should load data on init', () => {
    spyOn(tableDataService, 'getTableData').and.callFake(() => of((jsonFile as any).default));
    component.ngOnInit();
    expect(component.columnNames.length).toBeGreaterThan(0);
    expect(component.itemsPerPage.value).toEqual(10);
    expect(component.pageNo.value).toEqual(1);
    expect(component.tableData.length).toBeGreaterThan(0);
  });

});
